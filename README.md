# NodeJS script that connects to a Palo Alto Networks firewall and performs a reboot.

Requires the installation of module: xml2js
$ sudo npm install -g xml2js

If you receive errors that module xml2js cannot be found, that means your environment variables are not set up to find the globally installed packages.
You can add the path (NODE_PATH) to where the modules are installed (varies by distribution) 
Otherwise, in the folder where the reboot.js file resides, you can just insall xml2js again without the -g flag (global install flag).

To create an API on the firewall:
https://fw_ip_address/api/?type=keygen&user=admin&password=admin

You can also create unique accounts with specialized / restricted permissions if you don't want to associate with the admin account. To do this, create a new admin role and restrict SSH / GUI access and allow API to only operational commands. Then, craete a new user and attach this role to the user and commit. Generate the api key and modify the script with the key.

If you want to reboot at a specific time:
$ crontab -e
0 4 28 3 * /usr/bin/node /path/to/reboot.js

This will reboot on March 28 at 4am. Make sure you remove the crontab after since some cron's don't support the year extension
