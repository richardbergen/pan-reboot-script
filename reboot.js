process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0; // disable untrusted cert errors
const https = require('https');
let parseString = require('xml2js').parseString;
const StringDecoder = require('string_decoder').StringDecoder;

/*
See README.md for instructions.
*/

const fwAPI = 'insert_fw_api_key_here';
const fwIP = '1.2.3.4';
const cmd = '<request><restart><system><%2Fsystem><%2Frestart><%2Frequest>';
let output = '';    


function httpGet() {
    var optionsgetmsg = {
        host : fwIP, // here only the domain name
        port : 443,
        path : `/api/?type=op&cmd=${cmd}&key=${fwAPI}`, // the rest of the url with parameters if needed
        method : 'GET' // do GET
    };

    var reqGet = https.request(optionsgetmsg, function(resp) {
        resp.on('data', function(d) {
            output += d.toString(); //take all the output 
        });
        resp.on('end', function() {
            parseString(output, { explicitArray : false }, function(err,result){
                //console.log(result.response.result); // to see raw data
            });
        });
    });

    reqGet.end();
    reqGet.on('error', function(e) {
        console.error(`Error: ${e}`);
    });

}
httpGet();
